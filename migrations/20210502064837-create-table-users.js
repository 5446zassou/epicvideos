// var dbm = global.dbm
// var type = dbm.dataType;
var fs = require('fs');
var path = require('path');
var migration_name = path.basename(__filename).match(/^(\S+)\.js/)[1]

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
// exports.setup = function(options, seedLink) {
//   dbm = options.dbmigrate;
//   type = dbm.dataType;
//   seed = seedLink;
// };

exports.up = function(db, callback) {
  var filePath = path.join(__dirname + '/sqls/' + migration_name + '-up.sql');
  fs.readFile(filePath, {encoding: 'utf-8'}, function(err, data){
    if (err) return callback(err);

    db.runSql(data, function(err) {
      if (err) return callback(err);
      callback();
    });
  });
};

exports.down = function(db) {
  var filePath = path.join(__dirname + '/sqls/' + migration_name + '-down.sql');
  fs.readFile(filePath, {encoding: 'utf-8'}, function(err,data){
    if (err) return callback(err);

    db.runSql(data, function(err) {
      if (err) return callback(err);
      callback();
    });
  });

};

// exports._meta = {
//   "version": 1
// };
