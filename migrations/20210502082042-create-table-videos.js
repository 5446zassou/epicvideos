// var dbm = global.dbm
// var type = dbm.dataType;
var fs = require('fs');
var path = require('path');
var migration_name = path.basename(__filename).match(/^(\S+)\.js/)[1]

exports.up = function(db, callback) {
  var filePath = path.join(__dirname + '/sqls/' + migration_name + '-up.sql');
  fs.readFile(filePath, {encoding: 'utf-8'}, function(err,data){
    if (err) return callback(err);

    db.runSql(data, function(err) {
      if (err) return callback(err);
      callback();
    });
  });
};

exports.down = function(db, callback) {
  var filePath = path.join(__dirname + '/sqls/' + migration_name + '-down.sql');
  fs.readFile(filePath, {encoding: 'utf-8'}, function(err,data){
    if (err) return callback(err);

    db.runSql(data, function(err) {
      if (err) return callback(err);
      callback();
    });
  });
};
