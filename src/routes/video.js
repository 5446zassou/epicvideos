var express = require('express');
const VideoController = require('../controllers/VideoController');
var router = express.Router();

/* videos page. */
router.get('/create', VideoController.createVideoGet);
router.post('/create', VideoController.createVideoPost);
router.get('/list', VideoController.getVideos);
router.get('/update/:videoId', VideoController.updateVideoGet);
router.post('/update/:videoId', VideoController.updateVideoPost);
router.post('/delete/:videoId', VideoController.deleteVideo);
router.get('/:videoId', VideoController.getVideo);
module.exports = router;
