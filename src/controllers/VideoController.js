const express = require('express');
const Video = require('../models/Video');
const views = '../views/'

module.exports = {
  getVideo: function (req, res, next) {
    Video.getVideo(req.params.videoId).then((result) => {
      res.render(views + 'video.pug',{video: result[0]});
    });
  },
  getVideos: function (req, res, next) {
    Video.getVideos().then((result) => {
      res.render(views + 'videos.pug',{videos: result});
    });
  },
  createVideoGet: function (req, res, next) {
    res.render(views + 'video_form.ejs');
  },
  createVideoPost: function (req, res, next) {
    Video.createVideo(req.body)
    res.redirect('/');
  },
  updateVideoGet: function (req, res, next) {
    Video.getVideo(req.params.videoId).then((result) => {
      res.render(views + 'update.ejs',{video: result[0]});
    });
  },
  updateVideoPost: function (req, res, next) {
    Video.updateVideo(req)
    res.redirect('/');
  },
  deleteVideo: function (req, res, next) {
    Video.deleteVideo(req.params.videoId)
    res.redirect('/');
  }
}　