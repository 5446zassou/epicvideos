const mysql = require('mysql2');
const table = 'videos';
const mysqlSetting = {
  host: 'mysql',
  port: 3306,
  user: 'root',
  password: 'qwert123',
  database: 'epicvideos'
};

module.exports = {

  getVideo: function (videoId) {
    const con = mysql.createConnection(mysqlSetting);

    return new Promise ((resolve, reject) => {
      con.query(
        `SELECT * FROM ${table} WHERE id = ${videoId}`,  (err, result, fields) => {
          if ( err ) {
            reject(err);
          } else {
            resolve(result);
          }
        });
      con.end();
    });
  },
  createVideo: function (params) {
    const con = mysql.createConnection(mysqlSetting);

    con.query(
      `INSERT INTO ${table} (content, video_url, image, created, modified, updated) VALUES("${params.content}", "${params.url}", "${params.image}", now(), now(), now());`);
    con.end();
  },
  updateVideo: function (data) {
    const con = mysql.createConnection(mysqlSetting);
    const id = data.params.videoId
    const info = {
      'content': data.body.content,
      'video_url': data.body.url,
      'image': data.body.image,
    }

    con.query(`UPDATE ${table} SET ? WHERE id = ${id}`, info);
    con.end();
  },
  deleteVideo: function (id) {
    const con = mysql.createConnection(mysqlSetting);

    con.query(`DELETE FROM ${table} WHERE id = ${id}`);
    con.end();
  },
  getVideos: function () {
    const con = mysql.createConnection(mysqlSetting);

    return new Promise ((resolve, reject) => {
      con.query(
        `SELECT * FROM ${table}`,  (err, result, fields) => {
          if ( err ) {
            reject(err);
          } else {
            resolve(result);
          }
        });
      con.end();
    });
  }
}